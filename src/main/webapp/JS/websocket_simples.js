
window.onload = conectar;

var websocket;
//------------------------------------------------------------------------------
function conectar() {
    var wsUri = "ws://"+window.location.href.toString().slice(5)+"soquete";
    try {
        websocket = new WebSocket(wsUri);
        
    } catch (erro) {
        $('#idMensagens').html('Erro: ' + erro);
        return;
    }
    websocket.binaryType = "arraybuffer";

    websocket.onopen = function(ev){console.log('=== Conectou');}
    
    var vBot = document.getElementsByName('botaoSubmit');
    for (var i=0; i<vBot.length; i++){
        vBot[i].addEventListener('click',function(x){
                                            websocket.send(x.target.parentNode.id);
                                            document.getElementById('im'+x.target.parentNode.id).style = "background-image:url('../trabalho-henrique/images/ImgGirDes.jpg'); \n\
                                            width: 180px; height: 180px; margin-right: auto; margin-left: auto; border-radius: 15px;";
                                        });
    }
    
    websocket.onmessage = function (evt) {
        var json = JSON.parse(evt.data);
        console.log(json.mensagem);
        var image = document.getElementById('im'+json.mensagem).style.backgroundImage;
        console.log(image);
        if (typeof evt.data === "string") {
            if(image=="url(\"../trabalho-henrique/images/ImgGirVer1.jpg\")"){
                document.getElementById('im'+json.mensagem).style = "background-image:url('../trabalho-henrique/images/ImgGirVer2.jpg'); \n\
                                                                    width: 180px; height: 180px; margin-right: auto; margin-left: auto; border-radius: 15px;";
            }else if(image=="url(\"../trabalho-henrique/images/ImgGirVer2.jpg\")"){
                document.getElementById('im'+json.mensagem).style = "background-image:url('../trabalho-henrique/images/ImgGirVer1.jpg'); \n\
                                                                    width: 180px; height: 180px; margin-right: auto; margin-left: auto; border-radius: 15px;";
            }else{
                document.getElementById('im'+json.mensagem).style = "background-image:url('../trabalho-henrique/images/ImgGirVer1.jpg'); \n\
                                                                    width: 180px; height: 180px; margin-right: auto; margin-left: auto; border-radius: 15px;";
            }
        } else {
            console.log('Recebeu dados binários! E agora?');
        }
    };
    websocket.onerror = function (evt) {
        $('#idMensagens').html('Erro: ' + evt);
    };
}
//------------------------------------------------------------------------------
function desconectar() {
    websocket.close();
    $('#idMensagens').html('DESCONECTOU!');
}
//------------------------------------------------------------------------------
