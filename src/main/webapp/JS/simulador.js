
window.onload = main;
function main(){
    var vBot = document.getElementsByName('botaoSubmit');
    for (var i=0; i<vBot.length; i++){
        vBot[i].addEventListener('click', fazerPedidoAJAX);
    }
}


//------------------------------------------------------------------------------

function fazerPedidoAJAX(x){
    var id = x.target.parentNode.id
    switch (x.target.value){
        case "Alarmar":
            var objetoJSON = {"comando":"alarmar", "id":id};
            x.target.value = "Desalarmar";
            break;
        case "Desalarmar":
            var objetoJSON = {"comando":"desalarmar", "id":id};
            x.target.value = "Alarmar";
            break;
    }
    var stringJSON = JSON.stringify(objetoJSON);
    var objPedidoAJAX = new XMLHttpRequest();
    objPedidoAJAX.open("POST", "servlet");
    objPedidoAJAX.setRequestHeader("Content-Type","application/json;charset=UTF-8");
    // Prepara recebimento da resposta: tipo da resposta JSON!
    objPedidoAJAX.responseType = 'json';
    
    objPedidoAJAX.onreadystatechange =
        function() {
            if(objPedidoAJAX.readyState===4 && objPedidoAJAX.status===200){
                // A resposta 'response' já é avaliada para json!
                // Ao contraro da resposta responseText.
                var respJSON = objPedidoAJAX.response;
                console.log(respJSON);

            };
        };
        
    // Envio do pedido
    objPedidoAJAX.send(stringJSON);
}
//------------------------------------------------------------------------------
