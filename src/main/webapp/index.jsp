<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Minha Casa</title>
        <link href="https://fonts.googleapis.com/css?family=Raleway:400,700" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="css/indexV2.css" />
        <script src="JS/index.js"></script>
        <script src="JS/utilidades.js"></script>
        <script> listaScript=new Array();</script>
    </head>
    

    <body>
        <% if(request.getSession().getAttribute("AUTENTICADO")=="false"){%>
            <meta http-equiv="refresh" content="0.000000001; url=login.jsp">
        <%}else{%>
        <div id="pagina0" class="divMaisExterna">
                <div class="divSuperior">
                    <input type="submit" name="botaoSubmit" value="Adicionar" class="botao"/>
                    <input type="submit" name="botaoSubmit" value="Editar" class="botao"/>
                    <input type="submit" name="botaoSubmit" value="Sair" class="botao"/>
                </div>
                <br>   
                <br>   
                <span class="titulo">${sessionScope.NOME_CASA}</span>
                <br>   
                <br> 
                <span class="titulo">Ambiente</span>
                <br>
                <br>
                <div id="div2">
                    <select name="selectAmbiente"><option value='000'>Selecione Ambiente</option></select>
                </div>
                
        </div>
        <div id="pagina1" class="divMaisExterna">
                <div class="divSuperior">
                    <input type="submit" name="botaoSubmit" value="Início" class="botao"/>
                    <input type="submit" name="botaoSubmit" value="Sair" class="botao"/>
                </div>
                <br>   
                <br>   
                <span class="titulo">${sessionScope.NOME_CASA}</span>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <div class="divLogin">
                    <br>
                        <br>
                        <input id="tituloLogin" type="text" name="tituloLogin" class="titulo" value="Novo Ambiente" readOnly/>
                        <br>
                        <br>
                        <div class="textPadrao0">
                            <input type="text" name="ambienteNome" class="textPadrao"
                                value="" placeholder="Nome do Ambiente" />
                        </div>
                        <br>
                        <input type="submit" name="botaoSubmit" value="Salvar" class="botao"/>
                        <br>
                        <input id="tituloLogin" type="text" name="tituloLogin" class="titulo" value="Novo Dispositivo" readOnly/>
                        <br>
                        <br>
                        <div class="textPadrao0">
                            <input type="text" name="dispositivoNome" class="textPadrao"
                                value="" placeholder="Nome do dispositivo" />
                        </div>
                        <br>
                        <div class="textPadrao0">
                            <input type="text" name="dispositivoComando" class="textPadrao"
                                value="" placeholder="Comando do dispositivo" />
                        </div>
                        <br>
                        <div>
                            <select name="dispositivoAmbiente">
                                <option value="000">Selecione Ambiente</option>
                            </select>
                        </div>
                        <br>
                        
                        
                    <input type="submit" name="botaoSubmit" value="Criar" class="botao"/>
                    <br>                
                </div>
        </div>
        <div id="pagina2" class="divMaisExterna">
            <div class="divSuperior">
                <input type="submit" name="botaoSubmit" value="Início" class="botao"/>
                <input type="submit" name="botaoSubmit" value="Sair" class="botao"/>
            </div>
            <br>   
            <br>   
            <span class="titulo">${sessionScope.NOME_CASA}</span>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <div class="divLogin">
                <br>
                    <br>
                    <input id="tituloLogin" type="text" name="tituloLogin" class="titulo" value="Editar Ambiente" readOnly/>
                    <br>
                    <br>
                    <div>
                        <select name="ambienteId">
                            <option value="000">Selecione Ambiente</option>
                        </select>
                    </div>
                        <br>
                    <div class="textPadrao0">
                        <input type="text" name="ambienteNome" class="textPadrao"
                            value="" placeholder="Novo nome do ambiente" />
                    </div>
                    <br>
                    <input type="submit" name="botaoSubmit" value="Excluir Ambiente" class="botao"/>
                    <input type="submit" name="botaoSubmit" value="Salvar Ambiente" class="botao"/>
                    <br>
                    <input id="tituloLogin" type="text" name="tituloLogin" class="titulo" value="Editar Dispositivo" readOnly/>
                    <br>
                    <br>
                    <div>
                        <select name="dispositivoAmbiente">
                            <option value="000">Selecione Ambiente</option>
                        </select>
                    </div>
                    <br>
                    <div id="criarSelect" style="margin-left: auto; margin-right: auto;"></div>
                    <br>
                    <div class="textPadrao0">
                        <input type="text" name="dispositivoNome" class="textPadrao"
                            value="" placeholder="Novo nome do dispositivo" />
                    </div>
                    <br>
                    <div class="textPadrao0">
                        <input type="text" name="dispositivoComando" class="textPadrao"
                            value="" placeholder=" Novo comando do dispositivo" />
                    </div>
                    <br>
                    <div>
                        <select name="dispositivoStatus">
                            <option value="0">Status: Desligado</option>
                            <option value="1">Status: Ligado</option>
                        </select>
                    </div>
                    <br>
                    <div>
                        <select name="dispositivoAmbiente">
                            <option value="000">Selecione Ambiente</option>
                        </select>
                    </div>
                    <br>

                <input type="submit" name="botaoSubmit" value="Excluir Dispositivo" class="botao"/>
                <input type="submit" name="botaoSubmit" value="Salvar Dispositivo" class="botao"/>
                <br>
            </div>
    </div>
        <div id="pagina3" class="divMaisExterna">
                <div class="divSuperior">
                    <input type="submit" name="botaoSubmit" value="Adicionar" class="botao"/>
                    <input type="submit" name="botaoSubmit" value="Editar" class="botao"/>
                    <input type="submit" name="botaoSubmit" value="Sair" class="botao"/>
                </div>
                <br>   
                <br>   
                <span class="titulo">${sessionScope.NOME_CASA}</span>
                <br>   
                <br> 
                <span class="titulo">Ambiente</span>
                <br>
                <br>
                <div id="div2">
                    <select name="selectAmbiente"></select>
                </div>

                <br>
                <br>
                <br>
                <span class="titulo">Dispositivo</span>
                <br>
                <br>
                <div id="divListBox"></div>
        </div>
<%}%>
    </body>
</html>

