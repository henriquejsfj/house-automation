CREATE TABLE "casa" (
  "idCasa" serial NOT NULL PRIMARY KEY,
  "Nome" char(50) NOT NULL,
  "Cep" char(8) DEFAULT NULL,
  "Rua" char(100) DEFAULT NULL,
  "Numero" int DEFAULT NULL
) ;

CREATE TABLE "ambiente" (
  "idAmbiente" serial NOT NULL PRIMARY KEY,
  "Nome" char(50) NOT NULL unique,
  "Casa_idCasa" int NOT NULL,
CONSTRAINT "fk_Ambiente_Casa_idx" FOREIGN KEY ("Casa_idCasa") REFERENCES casa("idCasa")
ON DELETE CASCADE ON UPDATE CASCADE

) ;

CREATE TABLE "dispositivo" (
  "idDispositivo" serial NOT NULL PRIMARY KEY,
  "Nome" char(50) NOT NULL,
  "Comando" varchar(100) NOT NULL,
  "Ambiente_idAmbiente" int NOT NULL,
  CONSTRAINT "fk_Dispositivo_Ambiente1_idx" FOREIGN KEY ("Ambiente_idAmbiente") REFERENCES ambiente("idAmbiente")
  ON DELETE CASCADE ON UPDATE CASCADE

) ;

CREATE TABLE "usuario" (
  "idUsuario" serial NOT NULL PRIMARY KEY,
  "Nome" char(50) NOT NULL,
  "Senha" varchar(45) NOT NULL,
  "Casa_idCasa" int NOT NULL,
  CONSTRAINT "fk_Usuario_Casa1_idx" FOREIGN KEY ("Casa_idCasa") REFERENCES casa("idCasa")
  ON DELETE CASCADE ON UPDATE CASCADE
) ;
