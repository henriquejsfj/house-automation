<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Minha Casa</title>
        <link href="https://fonts.googleapis.com/css?family=Raleway:400,700" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="css/editar.css" />        
    </head>

    <body>
        <div id="divMaisExterna">
            <form method="GET" action="controllerV2">
            <input type="hidden"
                   name="nomeDoTratadorDePagina"
                   value="henrique.pagehandlers.Tratador_editar_jsp" />
            <% if(request.getSession().getAttribute("AUTENTICADO")=="false"){%>
            <meta http-equiv="refresh" content="0.000000001; url=login.jsp">
            <%}else{%>
                <div id="divSuperior">
                    <input type="submit" name="botaoSubmit" value="Início" class="botao"/>
                    <input type="submit" name="botaoSubmit" value="Sair" class="botao"/>
                </div>
                <br>   
                <br>   
                <span class="titulo">${sessionScope.NOME_CASA}</span>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <div id="divLogin">
                    <br>
                        <br>
                        <input id="tituloLogin" type="text" name="tituloLogin" class="titulo" value="Editar Ambiente" readOnly/>
                        <br>
                        <br>
                        <div>
                            <select name="ambienteId">
                                <option value="000">Selecione Ambiente</option>
                                <%  AcessoMySQL.AmbienteDAO sqlAmbiente = new AcessoMySQL.AmbienteDAO();
                                AcessoMySQL.CasaDAO sqlCasa = new AcessoMySQL.CasaDAO();
                                Objetos.Casa casa = new Objetos.Casa();
                                casa.setId(Integer.parseInt(request.getSession().getAttribute("CASA").toString()));
                                sqlCasa.Busca(casa);
                                for (Objetos.Ambiente ambiente : sqlAmbiente.Busca_porCasa(casa)){
                                    if (request.getSession().getAttribute("AMBIENTE").equals(Integer.toString(ambiente.getId()))){
                                        %> <option value= "<% out.print(ambiente.getId()); %>" > <% out.print(ambiente.getNome()); %> </option>
                                    <%
                                    }
                                }
                                for (Objetos.Ambiente ambiente : sqlAmbiente.Busca_porCasa(casa)){
                                    if (!request.getSession().getAttribute("AMBIENTE").equals(Integer.toString(ambiente.getId()))){
                                        %> <option value= "<% out.print(ambiente.getId()); %>" > <% out.print(ambiente.getNome()); %> </option>
                                    <%
                                    }
                                }
                                %>
                            </select>
                        </div>
                            <br>
                        <div class="textPadrao0">
                            <input type="text" name="ambienteNome" class="textPadrao"
                                value="" placeholder="Novo nome do ambiente" />
                        </div>
                        <br>
                        <input type="submit" name="botaoSubmit" value="Excluir Ambiente" class="botao"/>
                        <input type="submit" name="botaoSubmit" value="Salvar/Escolher" class="botao"/>
                        <br>
                        <input id="tituloLogin" type="text" name="tituloLogin" class="titulo" value="Editar Dispositivo" readOnly/>
                        <br>
                        <br>
                        <div>
                            <select name="dispositivoId">
                                <option value="000">Selecione Dispositivo</option>
                                <%  AcessoMySQL.DispositivoDAO sqlDispositivo = new AcessoMySQL.DispositivoDAO();
                                Objetos.Ambiente ambiente = new Objetos.Ambiente();
                                ambiente.setId(Integer.parseInt(request.getSession().getAttribute("AMBIENTE").toString()));
                                for (Objetos.Dispositivo dispositivo : sqlDispositivo.Busca_porAmbiente(ambiente)){
                                    %>
                                     <option value= "<% out.print(dispositivo.getId()); %>" > <% out.print(dispositivo.getNome()); %> </option>
                                <%
                                }
                                %>
                            </select>
                        </div>
                        <br>
                        <div class="textPadrao0">
                            <input type="text" name="dispositivoNome" class="textPadrao"
                                value="" placeholder="Novo nome do dispositivo" />
                        </div>
                        <br>
                        <div class="textPadrao0">
                            <input type="text" name="dispositivoComando" class="textPadrao"
                                value="" placeholder=" Novo comando do dispositivo" />
                        </div>
                        <br>
                        <div>
                            <select name="dispositivoStatus">
                                <option value="0">Status: Desligado</option>
                                <option value="1">Status: Ligado</option>
                            </select>
                        </div>
                        <br>
                        <div>
                            <select name="dispositivoAmbiente">
                                <option value="000">Selecione Ambiente</option>
                                <%  
                                casa.setId(Integer.parseInt(request.getSession().getAttribute("CASA").toString()));
                                sqlCasa.Busca(casa);
                                for (Objetos.Ambiente ambiente1 : sqlAmbiente.Busca_porCasa(casa)){
                                    if (request.getSession().getAttribute("AMBIENTE").equals(Integer.toString(ambiente1.getId()))){
                                        %> <option value= "<% out.print(ambiente1.getId()); %>" > <% out.print(ambiente1.getNome()); %> </option>
                                    <%
                                    }
                                }
                                for (Objetos.Ambiente ambiente1 : sqlAmbiente.Busca_porCasa(casa)){
                                    if (!request.getSession().getAttribute("AMBIENTE").equals(Integer.toString(ambiente1.getId()))){
                                        %> <option value= "<% out.print(ambiente1.getId()); %>" > <% out.print(ambiente1.getNome()); %> </option>
                                    <%
                                    }
                                }
                                %>
                            </select>
                        </div>
                        <br>
                        
                    <input type="submit" name="botaoSubmit" value="Excluir Dispositivo" class="botao"/>
                    <input type="submit" name="botaoSubmit" value="Salvar" class="botao"/>
                    <br>
                    <span id="erro">${sessionScope.ERRO_CADASTRO}</span>

                
                </div>
             <%}%>   
            </form>
        </div>
    </body>
</html>


