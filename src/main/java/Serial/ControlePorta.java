
package Serial;

import gnu.io.CommPortIdentifier;
import gnu.io.NoSuchPortException;
import gnu.io.SerialPort;
import java.io.IOException;
import java.io.OutputStream;

public class ControlePorta {
  private static OutputStream serialOut;
  private static int taxa;
  private static String portaCOM;
  private static CommPortIdentifier portId;

  public static void ControlePorta() {
    
    portId = null;
    
  }     
 
  /**
   * Médoto que verifica se a comunicação com a porta serial está ok
   */
  public static void initialize(int taxa, String portaCOM) {
    ControlePorta.taxa = taxa;
    ControlePorta.portaCOM = portaCOM;
    try {
      //Define uma variável portId do tipo CommPortIdentifier para realizar a comunicação serial
      portId = null;
      try {
        //Tenta verificar se a porta COM informada existe
        portId = CommPortIdentifier.getPortIdentifier(ControlePorta.portaCOM);
      }catch (NoSuchPortException npe) {
        //Caso a porta COM não exista será exibido um erro 
        System.out.println("\n\n\nPorta não encontrada!\n\n\n");
      }
      //Abre a porta COM 
      SerialPort port = (SerialPort) portId.open("Comunicação serial", ControlePorta.taxa);
      serialOut = port.getOutputStream();
      port.setSerialPortParams(ControlePorta.taxa, //taxa de transferência da porta serial 
                               SerialPort.DATABITS_8, //taxa de 10 bits 8 (envio)
                               SerialPort.STOPBITS_1, //taxa de 10 bits 1 (recebimento)
                               SerialPort.PARITY_NONE); //receber e enviar dados

    }catch (Exception e) {
      e.printStackTrace();
    }
}

  /**
   * Método que fecha a comunicação com a porta serial
   */
  public static void close() {
    try {
        serialOut.close();
        System.out.println("\n\n\nfechou\n\n\n");
    }catch (IOException e) {
      System.out.println("Não foi possível fechar porta COM.");    }
  }

  /**
   * @param opcao - Valor a ser enviado pela porta serial
     * @return booleano se enviou ou n.
   */
  public static boolean enviaDados(int opcao){
    try {
      serialOut.write(opcao);
      return true;   //escreve o valor na porta serial para ser enviado
    } catch (IOException ex) {
        System.out.println("Não foi possível enviar o dado");
        return false;
    }
  } 
}