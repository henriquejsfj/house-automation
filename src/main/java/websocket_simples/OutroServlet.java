package websocket_simples;


import javax.websocket.server.ServerEndpoint;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.StringReader;
import java.util.LinkedList;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.websocket.Session;


public class OutroServlet 
extends HttpServlet {
    public static LinkedList<Boolean> ListaAlarme = new LinkedList<>();
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader((InputStream)request.getInputStream(), "UTF8"));
        String textoDoJson = br.readLine();
        JsonObject jsonObjectDeJava = null;
        try {
            JsonReader readerDoTextoDoJson = Json.createReader((Reader)new StringReader(textoDoJson));
            Throwable throwable = null;
            try {
                jsonObjectDeJava = readerDoTextoDoJson.readObject();
            }
            catch (Throwable var8_10) {
                throwable = var8_10;
                throw var8_10;
            }
            finally {
                if (readerDoTextoDoJson != null) {
                    if (throwable != null) {
                        try {
                            readerDoTextoDoJson.close();
                        }
                        catch (Throwable var8_9) {
                            throwable.addSuppressed(var8_9);
                        }
                    } else {
                        readerDoTextoDoJson.close();
                    }
                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        
//------------------------------------------------------------------------------------------------------------
        try (PrintWriter out = response.getWriter()) {
            out.print("");
        }
        int indice = Integer.parseInt(jsonObjectDeJava.getString("id"));
        switch (jsonObjectDeJava.getString("comando")){
            case "alarmar":{
                if (ListaAlarme.size()>indice){
                    if(ListaAlarme.get(indice)){
                        break;
                    }
                }
                for (int i=ListaAlarme.size(); i<=indice; i++){ListaAlarme.add(false);}
                ListaAlarme.set(indice, true);
                if (Soquete.ListaLista.size()>indice){Soquete.ListaLista.remove(indice);}
                for (int i=Soquete.ListaLista.size(); i<indice; i++){Soquete.ListaLista.add(new LinkedList<Session>());}
                Soquete.ListaLista.add(indice, (LinkedList<Session>) Soquete.sessions.clone());
                
                NovaThread Nova = new NovaThread(indice);
                Nova.start();
                //Soquete.Enviar(indice);
                break;
            }case "desalarmar":{
                System.out.println("desalarmou?");
                ListaAlarme.set(indice, false);
                System.out.println(ListaAlarme.get(indice));
                break;
            }
        }
        //websocket_simples.OutroServlet.session.getBasicRemote().sendText(
        //                           "{\"mensagem\":\"Resposta do servlet pelo websocket!\"}");
    }
    public class NovaThread extends Thread{
        int indice;
        public NovaThread( int indice) {
            this.indice = indice;
        }
        @Override
        public void run(){
            Soquete.Enviar(this.indice);
        }        
    }
/*    
    //==== TEM QUE SER STATIC PARA DURAR POR VÁRIOS PEDIDOS! ====
    public static Session session;
        
    @OnOpen
    public void onOpen(Session session) {
        try {
            OutroServlet.session = session;
            OutroServlet.session.getBasicRemote().sendText("{\"mensagem\":\"CONECTOU!\"}");
            System.out.println("--- OnOpen: Conectou.");
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    @OnMessage
    public void receber(String arg){

    }

    @OnError
    public void onError(Session session,java.lang.Throwable throwable) {
        System.out.println("--- OnError: "+throwable);
    }

    @OnClose
    public void onClose(Session session,CloseReason reason) {
        System.out.println("--- OnClose: "+reason);
    }
*/    
    
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
