package websocket_simples;

import java.io.IOException;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.websocket.CloseReason;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

@ServerEndpoint("/soquete")
public class Soquete extends Thread{
    
    public static LinkedList<Session> sessions = new LinkedList<>();
    public static LinkedList<LinkedList<Session>> ListaLista = new LinkedList<>();
        
    @OnOpen
    public void quandoAbrir(Session session) {
        try {
            Soquete.sessions.add(session);
            session.getBasicRemote().sendText("{\"mensagem\":\"MAOEE\"}");
            System.out.println(Soquete.sessions);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
    
    public static void Enviar(int i){
        int x = 0;
        while (!ListaLista.get(i).isEmpty()){
            try {
                for (Session session : ListaLista.get(i)){
                    session.getBasicRemote().sendText("{\"mensagem\":\""+i+"\"}");
                }
                Thread.sleep(2000);
            } catch (IOException ex) {
                Logger.getLogger(Soquete.class.getName()).log(Level.SEVERE, null, ex);
            }catch (InterruptedException ex){
                Logger.getLogger(Soquete.class.getName()).log(Level.SEVERE, null, ex);
            }
            x++;
        }
    }

    @OnMessage
    public void quandoReceber(Session session, String arg){
        System.out.println(session);
        System.out.println(OutroServlet.ListaAlarme.get(Integer.parseInt(arg)));
        if (!OutroServlet.ListaAlarme.get(Integer.parseInt(arg))){
            Soquete.ListaLista.get(Integer.parseInt(arg)).remove(session);
            System.out.println("\n\nChegay\n\n");
        }
    }

    @OnError
    public void quandoHouverErro(Session session,java.lang.Throwable throwable) {
        System.out.println("--- OnError: "+throwable);
    }

    @OnClose
    public void quandoFechar(Session session,CloseReason reason) {
//        System.out.println("--- OnClose: "+reason);
        Soquete.sessions.remove(session);
    }
}
