package henrique.drivers;

import java.util.LinkedList;
import javax.json.JsonObject;

public class Ar implements IFdosDrivers{
    
    public static LinkedList <Integer> idDisp = new LinkedList<>();
    public static LinkedList <Integer> statusDisp = new LinkedList<>();
    
    @Override
    public String processar(JsonObject jsonDoPedido){
        Integer id = Integer.parseInt(jsonDoPedido.getString("dispositivoId"));
        int index = idDisp.indexOf(id);
        String str = "{\"idDispositivo\":\""+id;
        if (index==-1){
            idDisp.add(id);
            statusDisp.add(0);
            return str+"\",\"status\":\"0\"}";
        }else{
            switch(jsonDoPedido.getString("botao")){
                case "Ligar":{
                    statusDisp.set(index, 1);
                    return str+"\",\"status\":\"1\"}";
                }case "Desligar":{
                    statusDisp.set(index, 0);
                    return str+"\",\"status\":\"0\"}";
                }default:{
                    return str+"\",\"status\":\""+statusDisp.get(index)+"\"}";
                }
                
            }
            
        }
    }
}
