package henrique.drivers;

import javax.json.JsonObject;

public interface IFdosDrivers {
    
    public String processar(JsonObject jsonDoPedido);
    
}
