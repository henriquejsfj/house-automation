
package henrique.pagehandlers;

import henrique.trabalho.IFTratadorDePaginas;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class Tratador_editar_jsp  implements IFTratadorDePaginas {
    @Override
    public String processar(HttpServletRequest request, HttpServletResponse response){
        String jspURL = "/editar.jsp";
        String botaoSubmit = request.getParameter("botaoSubmit");
        
        switch(botaoSubmit){
            case "Salvar/Escolher":{
                AcessoMySQL.AmbienteDAO sqlAmbiente = new AcessoMySQL.AmbienteDAO();
                Objetos.Ambiente ambiente = new Objetos.Ambiente();
                String ambienteNome = request.getParameter("ambienteNome");
                int ambienteId = Integer.parseInt(request.getParameter("ambienteId"));
                if (ambienteNome.equals("")){
                    request.getSession().setAttribute("AMBIENTE", ambienteId);
                }else{
                    ambiente.setNome(ambienteNome);
                    ambiente.setId(ambienteId);
                    sqlAmbiente.Atualizar(ambiente);
                }
                break;}
            case "Excluir Ambiente":{
                AcessoMySQL.AmbienteDAO sqlAmbiente = new AcessoMySQL.AmbienteDAO();
                Objetos.Ambiente ambiente = new Objetos.Ambiente();
                int ambienteId = Integer.parseInt(request.getParameter("ambienteId"));
                ambiente.setId(ambienteId);
                sqlAmbiente.Deletar(ambiente);
                break;}
            case "Salvar":{
                AcessoMySQL.DispositivoDAO sqlDispositivo = new AcessoMySQL.DispositivoDAO();
                Objetos.Dispositivo dispositivo = new Objetos.Dispositivo();
                Objetos.Dispositivo dispositivoRef = new Objetos.Dispositivo();
                dispositivoRef.setId(Integer.parseInt(request.getParameter("dispositivoId")));
                
                sqlDispositivo.Busca(dispositivoRef);
                dispositivo.setId(Integer.parseInt(request.getParameter("dispositivoId")));
                dispositivo.setNome(request.getParameter("dispositivoNome"));
                dispositivo.setComando(request.getParameter("dispositivoComando"));
                dispositivo.setStatus(Integer.parseInt(request.getParameter("dispositivoStatus")));
                dispositivo.setIdAmbiente(Integer.parseInt(request.getParameter("dispositivoAmbiente")));
                dispositivoRef.Atualiza(dispositivo);
                sqlDispositivo.Atualizar(dispositivoRef);
                break;}
            case "Excluir Dispositivo":{
                AcessoMySQL.DispositivoDAO sqlDispositivo = new AcessoMySQL.DispositivoDAO();
                Objetos.Dispositivo dispositivo = new Objetos.Dispositivo();
                int dispositivoId = Integer.parseInt(request.getParameter("dispositivoId"));
                dispositivo.setId(dispositivoId);
                sqlDispositivo.Deletar(dispositivo);
                break;}
            case "Início":{
                jspURL = "/index.jsp";
                break;}
            case "Sair":{
                jspURL = "/login.jsp";
                request.getSession().setAttribute("AUTENTICADO", "false");
                request.getSession().setAttribute("NOME_CASA", "Casa Loka");
                request.getSession().setAttribute("CASA", "-1");
                request.getSession().setAttribute("AMBIENTE", "-1");
                request.getSession().setAttribute("ERRO_CADASTRO", "");
                request.getSession().setAttribute("ERRO_LOGIN", "");
                break;}
        }
        return jspURL;
    }
}
