package henrique.pagehandlers;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import henrique.trabalho.IFTratadorDePaginas;

public class Tratador_index_jsp implements IFTratadorDePaginas {
    @Override
    public String processar(HttpServletRequest request, HttpServletResponse response){
        String jspURL = "/index.jsp";
        String botaoSubmit = request.getParameter("botaoSubmit");
        
        switch(botaoSubmit){
            case "Editar":{
                jspURL = "/editar.jsp";
                break;}
            case "Adicionar":{
                jspURL = "/adicionar.jsp";
                break;}
            case "Sair":{
                jspURL = "/login.jsp";
                request.getSession().setAttribute("AUTENTICADO", "false");
                request.getSession().setAttribute("NOME_CASA", "Casa Loka");
                request.getSession().setAttribute("CASA", "-1");
                request.getSession().setAttribute("AMBIENTE", "-1");
                request.getSession().setAttribute("ERRO_CADASTRO", "");
                request.getSession().setAttribute("ERRO_LOGIN", "");
                break;}
            
            default:{
                request.getSession().setAttribute("AMBIENTE", botaoSubmit);
                jspURL = "/listados.jsp";
            }
        }
        return jspURL;
    }
}

















/*        
        if(botaoSubmit.equals("Salvar")){
        //--- Salvar nome da casa no BD... (parte do modelo)
        
        }else if(botaoSubmit.equals("Editar") || botaoSubmit.equals("Editando") || botaoSubmit.isEmpty()){ //-- Redundante, só para documentar...
            readonly = ((String)request.getSession().getAttribute("READ_ONLY")).trim();
            if(readonly==null){
                readonly = "readonly";
                request.getSession().setAttribute("READ_ONLY", "readonly");
                request.getSession().setAttribute("COLOR", "white");
                request.getSession().setAttribute("EDITAR", "Editar");
            }else if(readonly.equals("")){    
                readonly = "readonly";
                request.getSession().setAttribute("READ_ONLY", "readonly");
                request.getSession().setAttribute("COLOR", "white");
                request.getSession().setAttribute("EDITAR", "Editar");
            }else{
                readonly = "";
                request.getSession().setAttribute("READ_ONLY", "");
                request.getSession().setAttribute("COLOR", "red");
                request.getSession().setAttribute("EDITAR", "Editando");
            }
        }
*/        

