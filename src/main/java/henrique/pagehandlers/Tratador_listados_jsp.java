package henrique.pagehandlers;

import Serial.ControlePorta;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import henrique.trabalho.IFTratadorDePaginas;
import java.util.Enumeration;

public class Tratador_listados_jsp implements IFTratadorDePaginas {
    @Override
    public String processar(HttpServletRequest request, HttpServletResponse response){
        
        String jspURL = "/listados.jsp";
        String botaoSubmit = request.getParameter("botaoSubmit");
        String id="";
        for (Enumeration<String> test = request.getParameterNames(); test.hasMoreElements();){
            id = test.nextElement();
        }
        if (!(id.equals("botaoSubmit")||id.equals(""))){
            int valor;
            if (request.getParameter(id).equals("    Ligar")){
                valor = 1;
            }else{
                valor = 0;
            }
            
            //arduino = new Serial.ControlePorta("COM8",9600);
            
            AcessoMySQL.DispositivoDAO sqlDispositivo = new AcessoMySQL.DispositivoDAO();
            Objetos.Dispositivo dispositivo = new Objetos.Dispositivo();
            dispositivo.setId(Integer.parseInt(id));
            sqlDispositivo.Busca(dispositivo);
            //if (ControlePorta.enviaDados(dispositivo.getComando().charAt(0))){
                sqlDispositivo.AtualizarStatus(Integer.parseInt(id), valor);
            //}
        }
        switch(botaoSubmit){
            case "Adicionar":{
                jspURL = "/adicionar.jsp";
            }
            case "Editar":{
                jspURL = "/editar.jsp";
                break;}
            
            case "Sair":{
                jspURL = "/login.jsp";
                request.getSession().setAttribute("AUTENTICADO", "false");
                request.getSession().setAttribute("NOME_CASA", "Casa Loka");
                request.getSession().setAttribute("CASA", "-1");
                request.getSession().setAttribute("AMBIENTE", "-1");
                request.getSession().setAttribute("ERRO_CADASTRO", "");
                request.getSession().setAttribute("ERRO_LOGIN", "");
                break;}
            
            default:{
                request.getSession().setAttribute("AMBIENTE", botaoSubmit);
                jspURL = "/listados.jsp";
            }
        }
        return jspURL;
    }
}

















/*        
        if(botaoSubmit.equals("Salvar")){
        //--- Salvar nome da casa no BD... (parte do modelo)
        
        }else if(botaoSubmit.equals("Editar") || botaoSubmit.equals("Editando") || botaoSubmit.isEmpty()){ //-- Redundante, só para documentar...
            readonly = ((String)request.getSession().getAttribute("READ_ONLY")).trim();
            if(readonly==null){
                readonly = "readonly";
                request.getSession().setAttribute("READ_ONLY", "readonly");
                request.getSession().setAttribute("COLOR", "white");
                request.getSession().setAttribute("EDITAR", "Editar");
            }else if(readonly.equals("")){    
                readonly = "readonly";
                request.getSession().setAttribute("READ_ONLY", "readonly");
                request.getSession().setAttribute("COLOR", "white");
                request.getSession().setAttribute("EDITAR", "Editar");
            }else{
                readonly = "";
                request.getSession().setAttribute("READ_ONLY", "");
                request.getSession().setAttribute("COLOR", "red");
                request.getSession().setAttribute("EDITAR", "Editando");
            }
        }
*/        

