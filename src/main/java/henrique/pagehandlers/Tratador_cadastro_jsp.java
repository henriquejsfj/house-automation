package henrique.pagehandlers;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import henrique.trabalho.IFTratadorDePaginas;

public class Tratador_cadastro_jsp implements IFTratadorDePaginas {
    public Objetos.Usuario usuario;
    public Objetos.Casa casa;
    @Override
    public String processar(HttpServletRequest request, HttpServletResponse response){
        String jspURL = "/cadastro.jsp";
        String botaoSubmit = request.getParameter("botaoSubmit");
        
        switch(botaoSubmit){
            case "Salvar":{
                AcessoMySQL.UsuarioDAO sqlUsuario = new AcessoMySQL.UsuarioDAO();
                AcessoMySQL.CasaDAO sqlCasa = new AcessoMySQL.CasaDAO();

                String nomeUsuario = request.getParameter("usuario");
                String senha = request.getParameter("senha");
                String senha2 = request.getParameter("senha2");
                if (!senha.equals(senha2)){
                    request.getSession().setAttribute("ERRO_CADASTRO", "Senhas devem ser iguais.");
                    return jspURL;
                }
                usuario = new Objetos.Usuario();
                usuario.setNome(nomeUsuario);
                usuario.setSenha(senha);
                if (sqlUsuario.VerificaNome(usuario)){
                    request.getSession().setAttribute("ERRO_CADASTRO", "Este nome já está sendo utilizado por outro usuário.");
                    return jspURL;
                }
                casa = new Objetos.Casa();
                casa.setNome(request.getParameter("casaNome"));
                casa.setCep(request.getParameter("casaCep"));
                casa.setRua(request.getParameter("casaRua"));
                if(!request.getParameter("casaNumero").equals("")){casa.setNumero(Integer.parseInt(request.getParameter("casaNumero")));}
                sqlCasa.Add(casa);
                sqlCasa.BuscaUltimo(casa);
                usuario.setIdCasa(casa.getId());
                sqlUsuario.Add(usuario);
                
                request.getSession().setAttribute("NOME_CASA", casa.getNome());
                request.getSession().setAttribute("AUTENTICADO", "true");
                request.getSession().setAttribute("CASA", Integer.toString(casa.getId()));
                request.getSession().setAttribute("ERRO_CADASTRO", "");
                jspURL = "/adicionar.jsp";
                break;}
            case "Sair":{
                jspURL = "/login.jsp";
                request.getSession().setAttribute("AUTENTICADO", "false");
                request.getSession().setAttribute("NOME_CASA", "Casa Loka");
                request.getSession().setAttribute("CASA", "-1");
                request.getSession().setAttribute("AMBIENTE", "-1");
                request.getSession().setAttribute("ERRO_CADASTRO", "");
                request.getSession().setAttribute("ERRO_LOGIN", "");
                break;}
            
            default:{}
        }
        return jspURL;
    }
}
