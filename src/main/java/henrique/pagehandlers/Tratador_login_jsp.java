package henrique.pagehandlers;

import Objetos.Casa;
import Objetos.Usuario;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import henrique.trabalho.IFTratadorDePaginas;

public class Tratador_login_jsp implements IFTratadorDePaginas {
    public Objetos.Usuario usuario;
    public Objetos.Casa casa;
    @Override
    public String processar(HttpServletRequest request, HttpServletResponse response){
        AcessoMySQL.UsuarioDAO sqlUsuario = new AcessoMySQL.UsuarioDAO();
        AcessoMySQL.CasaDAO sqlCasa = new AcessoMySQL.CasaDAO();
        String nomeUsuario = request.getParameter("usuario");
        String senha = request.getParameter("senha");
        String jspURL = "/login.jsp";
        String botaoSubmit = request.getParameter("botaoSubmit");
        Objetos.Usuario usuario = new Objetos.Usuario();
        Objetos.Casa casa = new Objetos.Casa();
        usuario.setNome(nomeUsuario);
        usuario.setSenha(senha);
        
        switch(botaoSubmit){
            case "Entrar":{
                if (sqlUsuario.Autentica(usuario)){
                    usuario.setSenha(null);
                    jspURL = "/index.jsp";
                    request.getSession().setAttribute("ERRO_LOGIN", "");
                    request.getSession().setAttribute("AUTENTICADO", "true");
                    casa.setId(usuario.getIdCasa());
                    sqlCasa.Busca(casa);
                    request.getSession().setAttribute("NOME_CASA", casa.getNome());
                    request.getSession().setAttribute("CASA", Integer.toString(usuario.getIdCasa()));
                }else{
                    request.getSession().setAttribute("ERRO_LOGIN", "Usuário ou senha incorretos.");
                    request.getSession().setAttribute("AUTENTICADO", "false");
                }
                break;}
            case "+ Criar Novo":{
                jspURL = "/cadastro.jsp";
                break;}
            
            default:{}
        }
        return jspURL;
    }
}
