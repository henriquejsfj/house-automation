package henrique.pagehandlers;


import henrique.trabalho.IFTratadorDePaginas;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class Tratador_adicionar_jsp  implements IFTratadorDePaginas{
    @Override
    public String processar(HttpServletRequest request, HttpServletResponse response){
        String jspURL = "/adicionar.jsp";
        String botaoSubmit = request.getParameter("botaoSubmit");
        
        switch(botaoSubmit){
            case "Salvar":{
                AcessoMySQL.AmbienteDAO sqlAmbiente = new AcessoMySQL.AmbienteDAO();
                Objetos.Ambiente ambiente = new Objetos.Ambiente();
                ambiente.setNome(request.getParameter("ambienteNome"));
                ambiente.setIdCasa(Integer.parseInt(request.getSession().getAttribute("CASA").toString()));
                sqlAmbiente.Add(ambiente);
                break;}
            case "Criar":{
                AcessoMySQL.DispositivoDAO sqlDispositivo = new AcessoMySQL.DispositivoDAO();
                Objetos.Dispositivo dispositivo = new Objetos.Dispositivo();
                dispositivo.setNome(request.getParameter("dispositivoNome"));
                dispositivo.setComando(request.getParameter("dispositivoComando"));
                dispositivo.setStatus(Integer.parseInt(request.getParameter("dispositivoStatus")));
                dispositivo.setIdAmbiente(Integer.parseInt(request.getParameter("dispositivoAmbiente")));
                sqlDispositivo.Add(dispositivo);
                break;}
            case "Início":{
                jspURL = "/index.jsp";
                break;}
            case "Sair":{
                jspURL = "/login.jsp";
                request.getSession().setAttribute("AUTENTICADO", "false");
                request.getSession().setAttribute("NOME_CASA", "Casa Loka");
                request.getSession().setAttribute("CASA", "-1");
                request.getSession().setAttribute("AMBIENTE", "-1");
                request.getSession().setAttribute("ERRO_CADASTRO", "");
                request.getSession().setAttribute("ERRO_LOGIN", "");
                break;}
        }
        return jspURL;
    }
}
