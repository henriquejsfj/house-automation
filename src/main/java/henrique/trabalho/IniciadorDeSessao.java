package henrique.trabalho;

import Serial.ControlePorta;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

public class IniciadorDeSessao implements HttpSessionListener {
    public AcessoMySQL.DispositivoDAO sqlDispositivo = new AcessoMySQL.DispositivoDAO();
    @Override
    public void sessionCreated(HttpSessionEvent se) {
        se.getSession().setAttribute("AUTENTICADO", "false");
        se.getSession().setAttribute("NOME_CASA", "Casa Loka");
        se.getSession().setAttribute("CASA", "-1");
        se.getSession().setAttribute("AMBIENTE", "-1");
        //ControlePorta.initialize(9600, "COM10");
    }

    @Override
    public void sessionDestroyed(HttpSessionEvent se) {
        //ControlePorta.close();
    }
}
