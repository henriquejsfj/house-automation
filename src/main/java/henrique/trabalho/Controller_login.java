/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  AcessoMySQL.CasaDAO
 *  AcessoMySQL.UsuarioDAO
 *  Objetos.Casa
 *  Objetos.Usuario
 *  henrique.trabalho.RespostaDTO
 *  javax.json.Json
 *  javax.json.JsonObject
 *  javax.json.JsonReader
 *  javax.servlet.ServletException
 *  javax.servlet.ServletInputStream
 *  javax.servlet.http.HttpServlet
 *  javax.servlet.http.HttpServletRequest
 *  javax.servlet.http.HttpServletResponse
 *  javax.servlet.http.HttpSession
 */
package henrique.trabalho;

import AcessoMySQL.CasaDAO;
import AcessoMySQL.UsuarioDAO;
import Objetos.Casa;
import Objetos.Usuario;
import henrique.trabalho.RespostaDTO;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.StringReader;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.servlet.ServletException;
import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class Controller_login
extends HttpServlet {
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader((InputStream)request.getInputStream(), "UTF8"));
        String textoDoJson = br.readLine();
        JsonObject jsonObjectDeJava = null;
        try {
            JsonReader readerDoTextoDoJson = Json.createReader((Reader)new StringReader(textoDoJson));
            Throwable throwable = null;
            try {
                jsonObjectDeJava = readerDoTextoDoJson.readObject();
            }
            catch (Throwable var8_10) {
                throwable = var8_10;
                throw var8_10;
            }
            finally {
                if (readerDoTextoDoJson != null) {
                    if (throwable != null) {
                        try {
                            readerDoTextoDoJson.close();
                        }
                        catch (Throwable var8_9) {
                            throwable.addSuppressed(var8_9);
                        }
                    } else {
                        readerDoTextoDoJson.close();
                    }
                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        
        RespostaDTO dto = new RespostaDTO();
        
        
        UsuarioDAO sqlUsuario = new UsuarioDAO();
        CasaDAO sqlCasa = new CasaDAO();
        String nomeUsuario = jsonObjectDeJava.getString("usuario");
        String senha = jsonObjectDeJava.getString("senha");
        Usuario usuario = new Usuario();
        Casa casa = new Casa();
        usuario.setNome(nomeUsuario);
        usuario.setSenha(senha);
     
        switch (jsonObjectDeJava.getString("botao")) {
            case "Entrar": {
                if (sqlUsuario.Autentica(usuario)) {
                    usuario.setSenha(null);
                    
                    request.getSession().setAttribute("AUTENTICADO", (Object)"true");
                    dto.setAutenticado(true);
                    casa.setId(usuario.getIdCasa());
                    sqlCasa.Busca(casa);
                    request.getSession().setAttribute("NOME_CASA", (Object)casa.getNome());
                    request.getSession().setAttribute("CASA", (Object)Integer.toString(usuario.getIdCasa()));
                    break;
                }
                dto.setErro_login(true);
                dto.setAutenticado(false);
                request.getSession().setAttribute("AUTENTICADO", (Object)"false");
                break;
            }
            case "Salvar": {
                String senha2 = jsonObjectDeJava.getString("senha2");
                if (!senha.equals(senha2)) {
                    dto.setErro_cadastro("Senhas devem ser iguais.");
                    break;
                }
                if (sqlUsuario.VerificaNome(usuario)) {
                    dto.setErro_cadastro("Este nome j\u00e1 est\u00e1 sendo utilizado por outro usu\u00e1rio.");
                    break;
                }
                casa.setNome(jsonObjectDeJava.getString("casaNome"));
                casa.setCep(jsonObjectDeJava.getString("casaCep"));
                casa.setRua(jsonObjectDeJava.getString("casaRua"));
                if (!jsonObjectDeJava.getString("casaNumero").equals("")) {
                    casa.setNumero(Integer.parseInt(request.getParameter("casaNumero")));
                }
                sqlCasa.Add(casa);
                sqlCasa.BuscaUltimo(casa);
                usuario.setIdCasa(casa.getId());
                sqlUsuario.Add(usuario);
                request.getSession().setAttribute("NOME_CASA", (Object)casa.getNome());
                request.getSession().setAttribute("AUTENTICADO", (Object)"true");
                dto.setAutenticado(true);
                request.getSession().setAttribute("CASA", (Object)Integer.toString(casa.getId()));
                break;
            }
        }
        
        response.setContentType("application/json;charset=UTF-8");
        PrintWriter out = response.getWriter();
        out.print(dto.toString());
        out.flush();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.processRequest(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.processRequest(request, response);
    }

    public String getServletInfo() {
        return "Short description";
    }
}
