
package henrique.trabalho;

import AcessoMySQL.AmbienteDAO;
import AcessoMySQL.CasaDAO;
import AcessoMySQL.DispositivoDAO;
import Objetos.Ambiente;
import Objetos.Casa;
import Objetos.Dispositivo;
import henrique.trabalho.RespostaDTO;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.StringReader;
import java.util.LinkedList;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.servlet.ServletException;
import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class Controller_index
extends HttpServlet {
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader((InputStream)request.getInputStream(), "UTF8"));
        String textoDoJson = br.readLine();
        JsonObject jsonObjectDeJava = null;
        try {
            JsonReader readerDoTextoDoJson = Json.createReader((Reader)new StringReader(textoDoJson));
            Throwable throwable = null;
            try {
                jsonObjectDeJava = readerDoTextoDoJson.readObject();
            }
            catch (Throwable var8_10) {
                throwable = var8_10;
                throw var8_10;
            }
            finally {
                if (readerDoTextoDoJson != null) {
                    if (throwable != null) {
                        try {
                            readerDoTextoDoJson.close();
                        }
                        catch (Throwable var8_9) {
                            throwable.addSuppressed(var8_9);
                        }
                    } else {
                        readerDoTextoDoJson.close();
                    }
                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        
        RespostaDTO dto = new RespostaDTO();
        if (request.getSession().getAttribute("AUTENTICADO").equals("true")){
            dto.setAutenticado(true);
        }else{
            response.setContentType("application/json;charset=UTF-8");
            PrintWriter out = response.getWriter();
            out.print(dto.toString());
            out.flush();
            return;
        }
        
        CasaDAO sqlCasa = new CasaDAO();
        Casa casa = new Casa();
        AmbienteDAO sqlAmbiente = new AmbienteDAO();
        Ambiente ambiente = new Ambiente();
        DispositivoDAO sqlDispositivo = new DispositivoDAO();
        Dispositivo dispositivo = new Dispositivo();
        switch (jsonObjectDeJava.getString("botao")) {
            case "Salvar": {
                ambiente.setNome(jsonObjectDeJava.getString("ambienteNome"));
                ambiente.setIdCasa(Integer.parseInt(request.getSession().getAttribute("CASA").toString()));
                sqlAmbiente.Add(ambiente);
                break;
            }
            case "Criar": {
                dispositivo.setNome(jsonObjectDeJava.getString("dispositivoNome"));
                dispositivo.setComando(jsonObjectDeJava.getString("dispositivoComando"));
                dispositivo.setIdAmbiente(Integer.parseInt(jsonObjectDeJava.getString("dispositivoAmbiente")));
                sqlDispositivo.Add(dispositivo);
                break;
            }
            case "Excluir Ambiente": {
                ambiente.setId(Integer.parseInt(jsonObjectDeJava.getString("ambienteId")));
                sqlAmbiente.Deletar(ambiente);
                break;
            }
            case "Salvar Ambiente": {
                ambiente.setId(Integer.parseInt(jsonObjectDeJava.getString("ambienteId")));
                ambiente.setNome(jsonObjectDeJava.getString("ambienteNome"));
                sqlAmbiente.Atualizar(ambiente);
                break;
            }
            case "Salvar Dispositivo": {
                Dispositivo dispositivoRef = new Dispositivo();
                dispositivoRef.setId(Integer.parseInt(jsonObjectDeJava.getString("dispositivoId")));
                sqlDispositivo.Busca(dispositivoRef);
                dispositivo.setId(Integer.parseInt(jsonObjectDeJava.getString("dispositivoId")));
                dispositivo.setNome(jsonObjectDeJava.getString("dispositivoNome"));
                dispositivo.setComando(jsonObjectDeJava.getString("dispositivoComando"));
                dispositivo.setIdAmbiente(Integer.parseInt(jsonObjectDeJava.getString("dispositivoAmbiente")));
                dispositivoRef.Atualiza(dispositivo);
                sqlDispositivo.Atualizar(dispositivoRef);
                break;
            }
            case "Excluir Dispositivo": {
                dispositivo.setId(Integer.parseInt(jsonObjectDeJava.getString("dispositivoId")));
                sqlDispositivo.Deletar(dispositivo);
                break;
            }
            case "Sair": {
                dto.setAutenticado(false);
                request.getSession().setAttribute("AUTENTICADO", (Object)"false");
                request.getSession().setAttribute("NOME_CASA", (Object)"Casa Loka");
                request.getSession().setAttribute("CASA", (Object)"-1");
                request.getSession().setAttribute("AMBIENTE", (Object)"-1");
                break;
            }
            case "Query":{
                casa.setId(Integer.parseInt(request.getSession().getAttribute("CASA").toString()));
                sqlCasa.Busca(casa);
                for (Objetos.Ambiente amb : sqlAmbiente.Busca_porCasa(casa)){
                    dto.addListaAmb(Integer.toString(amb.getId()));
                    dto.addListaAmb(amb.getNome());
                    for (Objetos.Dispositivo disp : sqlDispositivo.Busca_porAmbiente(amb)){
                        dto.addListaDisp(Integer.toString(amb.getId()));
                        dto.addListaDisp(Integer.toString(disp.getId()));
                        dto.addListaDisp(disp.getNome());
                        dto.addListaDisp(disp.getComando());
                    }
                }
                break;
            }
        }
        
        response.setContentType("application/json;charset=UTF-8");
        PrintWriter out = response.getWriter();
        out.print(dto.toString());
        out.flush();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.processRequest(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.processRequest(request, response);
    }

    public String getServletInfo() {
        return "Short description";
    }
}
