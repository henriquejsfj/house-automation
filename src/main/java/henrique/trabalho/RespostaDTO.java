/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  javax.json.Json
 *  javax.json.JsonObject
 *  javax.json.JsonObjectBuilder
 */
package henrique.trabalho;

import java.io.Serializable;
import java.util.LinkedList;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonArrayBuilder;

public class RespostaDTO
implements Serializable {
    private JsonArrayBuilder listaAmb = Json.createArrayBuilder();
    private JsonArrayBuilder listaDisp = Json.createArrayBuilder();
    private boolean erro_login = false;
    private boolean autenticado = false;
    private String erro_cadastro = "";
    private int status;
    private int idDispositivo;
    JsonObject objetoJSON;

    public JsonArrayBuilder getListaDisp() {
        return listaDisp;
    }

    public void addListaDisp(String element) {
        this.listaDisp.add(element);
    }
    
    public JsonArrayBuilder getListaAmb() {
        return listaAmb;
    }

    public void addListaAmb(String element) {
        this.listaAmb.add(element);
    }

    public String getErro_cadastro() {
        return erro_cadastro;
    }

    public void setErro_cadastro(String erro_cadastro) {
        this.erro_cadastro = erro_cadastro;
    }


    public boolean isErro_login() {
        return erro_login;
    }

    public void setErro_login(boolean erro_login) {
        this.erro_login = erro_login;
    }

    public boolean isAutenticado() {
        return autenticado;
    }

    public void setAutenticado(boolean autenticado) {
        this.autenticado = autenticado;
    }
    
    public int getStatus() {
        return this.status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getIdDispositivo() {
        return this.idDispositivo;
    }

    public void setIdDispositivo(int idDispositivo) {
        this.idDispositivo = idDispositivo;
    }

    public JsonObject getObjetoJSON() {
        return this.objetoJSON;
    }

    public void setObjetoJSON(JsonObject objetoJSON) {
        this.objetoJSON = objetoJSON;
    }

    public JsonObject toJSON() {
        this.objetoJSON = Json.createObjectBuilder().add("erro_login", this.erro_login
                                                   ).add("autenticado", this.autenticado
                                                   ).add("status", this.status
                                                   ).add("idDispositivo", this.idDispositivo
                                                   ).add("erro_cadastro", this.erro_cadastro
                                                   ).add("listaAmb", this.listaAmb
                                                   ).add("listaDisp", this.listaDisp
                                                   ).build();
        return this.objetoJSON;
    }

    public String toString() {
        return this.toJSON().toString();
    }
}
