
package Objetos;

public class Dispositivo {
    private int id;
    private String nome;
    private int status;
    private int idAmbiente;
    private String comando;
    public void Atualiza(Dispositivo dispositivo){
        if (dispositivo.getId()==this.id){
            this.status = dispositivo.getStatus();
            if (!dispositivo.getNome().equals("")){
                this.nome = dispositivo.getNome();
            }
            if (!dispositivo.getComando().equals("")){
                this.comando = dispositivo.getComando();
            }
            if (dispositivo.getIdAmbiente()!=0){
                this.idAmbiente = dispositivo.getIdAmbiente();
            }
        }
    }
    public String getComando() {
        return comando;
    }

    public void setComando(String comando) {
        this.comando = comando;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getIdAmbiente() {
        return idAmbiente;
    }

    public void setIdAmbiente(int idAmbiente) {
        this.idAmbiente = idAmbiente;
    }
}
