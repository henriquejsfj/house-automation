
package AcessoMySQL;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class DispositivoDAO extends Conexao {
    //private final Connection conn = ConexaoMySQL.getConexaoMySQL();
    private Connection conn;
    public static LinkedList <Objetos.Dispositivo> ListaDispositivos;
    public void Add(Objetos.Dispositivo dispositivo){
        conn = getConnection();
        String sql = "insert into dispositivo " +
                     "(\"Nome\", \"Comando\", \"Ambiente_idAmbiente\")" +
                     " values (?,?,?)";
        try {
            // prepared statement para inserção
            PreparedStatement stmt = conn.prepareStatement(sql);

            // seta os valores

            stmt.setString(1,dispositivo.getNome());
            stmt.setString(2, dispositivo.getComando());
            stmt.setInt(3,dispositivo.getIdAmbiente());

            // executa
            stmt.execute();
            stmt.close();
            conn.close();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
    public List<Objetos.Dispositivo> Busca_porAmbiente(Objetos.Ambiente ambiente){
        conn = getConnection();
        try {
            List<Objetos.Dispositivo> dispositivos = new ArrayList<Objetos.Dispositivo>();
            String sql = "SELECT \"idDispositivo\",\"Nome\",\"Comando\" " +
                     "FROM dispositivo " +
                     "WHERE \"Ambiente_idAmbiente\" = ? ";
            PreparedStatement stmt = this.conn.prepareStatement(sql);
            stmt.setInt(1,ambiente.getId());
            ResultSet rs = stmt.executeQuery();
            while (rs.next()){
                Objetos.Dispositivo dispositivo = new Objetos.Dispositivo();
                dispositivo.setId(rs.getInt("idDispositivo"));
                dispositivo.setNome(rs.getString("Nome"));
                dispositivo.setComando(rs.getString("Comando"));
                dispositivo.setIdAmbiente(ambiente.getId());
                dispositivos.add(dispositivo);
            }
         
            rs.close();
            stmt.close();
            conn.close();
            return dispositivos;
            
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }    
    }
    public void Busca(Objetos.Dispositivo dispositivo){
        try {
            conn = getConnection();
            String sql = "SELECT \"Nome\",\"Comando\", \"Ambiente_idAmbiente\"" +
                     "FROM dispositivo " +
                     "WHERE \"idDispositivo\" = ? ";
            PreparedStatement stmt = this.conn.prepareStatement(sql);
            stmt.setInt(1, dispositivo.getId());
            ResultSet rs = stmt.executeQuery();
            while (rs.next()){
                dispositivo.setNome(rs.getString("Nome"));
                dispositivo.setComando(rs.getString("Comando"));
                dispositivo.setIdAmbiente(rs.getInt("Ambiente_idAmbiente"));
            }
         
            rs.close();
            stmt.close();
            conn.close();
            
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }    
    }
    public void AtualizarStatus (int id, int valor){
        try {
            conn = getConnection();
            String sql = "UPDATE dispositivo " +
                         "SET \"Status\" = ? "+
                         "WHERE \"idDispositivo\" = ? ";
            PreparedStatement stmt = this.conn.prepareStatement(sql);
            stmt.setInt(1, valor);
            stmt.setInt(2, id);
            stmt.execute();
            stmt.close();
            conn.close();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }   
    }
    public void Atualizar (Objetos.Dispositivo dispositivo){
        try {
            conn = getConnection();
            String sql = "UPDATE dispositivo " +
                         "SET \"Nome\" = ?, " +
                         "\"Comando\" = ?, " +
                         "\"Ambiente_idAmbiente\" = ? " +
                         "WHERE \"idDispositivo\" = ? ";
            PreparedStatement stmt = this.conn.prepareStatement(sql);
            stmt.setString(1, dispositivo.getNome());
            stmt.setString(2, dispositivo.getComando());
            stmt.setInt(3, dispositivo.getIdAmbiente());
            stmt.setInt(4, dispositivo.getId());
            stmt.execute();
            stmt.close();
            conn.close();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }   
    }
    public void Deletar (Objetos.Dispositivo dispositivo){
        try {
            conn = getConnection();
            String sql = "DELETE FROM dispositivo " +
                         "WHERE \"idDispositivo\" = ? ";
            PreparedStatement stmt = this.conn.prepareStatement(sql);
            stmt.setInt(1, dispositivo.getId());
            stmt.execute();
            stmt.close();
            conn.close();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }   
    }
    public void Listar(){
        ListaDispositivos = new LinkedList<>();
        try {
            conn = getConnection();
            String sql = "SELECT * " +
                     "FROM dispositivo ";
            PreparedStatement stmt = this.conn.prepareStatement(sql);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()){
                Objetos.Dispositivo  dispositivo = new Objetos.Dispositivo();
                dispositivo.setNome(rs.getString("Nome"));
                dispositivo.setComando(rs.getString("Comando"));
                dispositivo.setIdAmbiente(rs.getInt("Ambiente_idAmbiente"));
                ListaDispositivos.add(dispositivo);
            }
         
            rs.close();
            stmt.close();
            conn.close();
            
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }    
    }
}
