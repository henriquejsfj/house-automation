package AcessoMySQL;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class CasaDAO extends Conexao {
    //private final Connection conn = ConexaoMySQL.getConexaoMySQL();
    private Connection conn;
    public void Add(Objetos.Casa casa){
        String sql = "insert into casa " +
                     "(\"Nome\", \"Cep\", \"Rua\", \"Numero\")" +
                     " values (?,?,?,?)";
        try {
            conn = getConnection();
            // prepared statement para inserção
            PreparedStatement stmt = conn.prepareStatement(sql);

            // seta os valores

            stmt.setString(1,casa.getNome());
            stmt.setString(2,casa.getCep());
            stmt.setString(3,casa.getRua());
            stmt.setInt(4,casa.getNumero());

            // executa
            stmt.execute();
            stmt.close();
            conn.close();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
    public void Busca (Objetos.Casa casa){
        try {
            conn = getConnection();
            String sql = "SELECT \"Nome\",\"Cep\",\"Rua\",\"Numero\" " +
                     "FROM casa " +
                     "WHERE \"idCasa\" = ? ";
            PreparedStatement stmt = this.conn.prepareStatement(sql);
            stmt.setInt(1,casa.getId());
            ResultSet rs = stmt.executeQuery();
            while (rs.next()){
                casa.setNome(rs.getString("nome"));
                casa.setCep(rs.getString("cep"));
                casa.setRua(rs.getString("rua"));
                casa.setNumero(rs.getInt("numero"));
            }
         
            rs.close();
            stmt.close();
            conn.close();
            
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }    
    }
    public void BuscaUltimo (Objetos.Casa casa){
        try {
            conn = getConnection();
            String sql = "SELECT MAX(\"idCasa\") as idCasa FROM \"casa\"";
            PreparedStatement stmt = this.conn.prepareStatement(sql);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()){
                casa.setId(rs.getInt("idCasa"));
            }
         
            rs.close();
            stmt.close();
            conn.close();
            
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }    
    }
}
