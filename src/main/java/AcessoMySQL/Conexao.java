package AcessoMySQL;

import java.sql.Connection;
import javax.naming.InitialContext;
import javax.sql.DataSource;

public class Conexao {

    private DataSource ds;

//------------------------------------------------------------------------------------------------------------
    public Conexao() {
        try {
            InitialContext cxt = new InitialContext();
            if (cxt == null) {
                System.out.println("[BaseDAO.constructor] Falha no InitialContext.");
            }else{
                ds = (DataSource) cxt.lookup("java:comp/env/jdbc/BancoDados");
            }
        } catch (Exception e) {
            System.out.println("[BaseDAO.constructor] Excessão: " + e.getMessage());
        }
    }
//------------------------------------------------------------------------------------------------------------
    public Connection getConnection(){
        try{
            if(ds!=null){
                return ds.getConnection();
            }else{
                return null;
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        return null;
    }
//------------------------------------------------------------------------------------------------------------
}
