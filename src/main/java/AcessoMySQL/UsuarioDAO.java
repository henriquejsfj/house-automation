package AcessoMySQL;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class UsuarioDAO extends Conexao {
    private String senha;
    private Connection conn;
    //private final Connection conn = ConexaoMySQL.getConexaoMySQL();
    public void Add(Objetos.Usuario usuario){
        conn = getConnection();
        String sql = "insert into usuario " +
                     "(\"Nome\", \"Senha\", \"Casa_idCasa\")" +
                     " values (?,?,?)";
        try {
            // prepared statement para inserção
            PreparedStatement stmt = conn.prepareStatement(sql);

            // seta os valores

            stmt.setString(1,usuario.getNome());
            stmt.setString(2,usuario.getSenha());
            stmt.setInt(3,usuario.getIdCasa());

            // executa
            stmt.execute();
            stmt.close();
            conn.close();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
    /**
     *
     * @param usuario
     * @return
     */
    public boolean Autentica (Objetos.Usuario usuario){
        conn = getConnection();
        try {
            
            String sql = "SELECT \"idUsuario\", \"Senha\", \"Casa_idCasa\" " +
                     "FROM usuario " +
                     "WHERE \"usuario\".\"Nome\" = ? ";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1,usuario.getNome());
            ResultSet rs = stmt.executeQuery();
            while (rs.next()){
                senha = rs.getString("senha");
                usuario.setIdCasa(rs.getInt("Casa_idCasa"));
                usuario.setId(rs.getInt("idUsuario"));
            }      
            rs.close();
            stmt.close();
            conn.close();
            if (null==senha){
                return false;
            }
            return senha.equals(usuario.getSenha());
        } catch (SQLException e) {
            return false;
        }
    }
    public boolean VerificaNome (Objetos.Usuario usuario){
        conn = getConnection();
        try {
            int id = 0;
            String sql = "SELECT \"idUsuario\" " +
                     "FROM usuario " +
                     "WHERE \"Nome\" = ?";
            PreparedStatement stmt = this.conn.prepareStatement(sql);
            stmt.setString(1,usuario.getNome());
            ResultSet rs = stmt.executeQuery();
            while (rs.next()){
                id = rs.getInt("idUsuario");
            }      
            rs.close();
            stmt.close();
            conn.close();
            return id != 0;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
    
}

    

