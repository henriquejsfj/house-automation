
package AcessoMySQL;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class AmbienteDAO extends Conexao {
    //private final Connection conn = ConexaoMySQL.getConexaoMySQL();
    private Connection conn;
    
    public void Add(Objetos.Ambiente ambiente){
        String sql = "insert into ambiente " +
                     "(\"Nome\",\"Casa_idCasa\")" +
                     " values (?,?)";
        try {
            conn = getConnection();
            // prepared statement para inserção
            PreparedStatement stmt = conn.prepareStatement(sql);

            // seta os valores

            stmt.setString(1,ambiente.getNome());
            stmt.setInt(2,ambiente.getIdCasa());

            // executa
            
            stmt.execute();
            stmt.close();
            conn.close();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
    public List<Objetos.Ambiente> Busca_porCasa(Objetos.Casa casa){
        try {
            conn = getConnection();
            List<Objetos.Ambiente> ambientes = new ArrayList<Objetos.Ambiente>();
            String sql = "SELECT \"idAmbiente\",\"Nome\" " +
                     "FROM ambiente " +
                     "WHERE \"Casa_idCasa\" = ? ";
            PreparedStatement stmt = this.conn.prepareStatement(sql);
            stmt.setInt(1, casa.getId());
            ResultSet rs = stmt.executeQuery();
            while (rs.next()){
                Objetos.Ambiente ambiente = new Objetos.Ambiente();
                ambiente.setId(rs.getInt("idAmbiente"));
                ambiente.setNome(rs.getString("Nome"));
                ambiente.setIdCasa(casa.getId());
                ambientes.add(ambiente);
            }
            conn.close();
            rs.close();
            stmt.close();
            return ambientes;
            
        } catch (SQLException e) {
            System.out.println("\n\n\n HEEEEEEEEEEEEEEYYYY \n\n\n");
            throw new RuntimeException(e);
        }    
    }
    public void Atualizar (Objetos.Ambiente ambiente){
        try {
            conn = getConnection();
            String sql = "UPDATE ambiente " +
                         "SET \"Nome\" = ? " +
                         "WHERE \"idAmbiente\" = ? ";
            PreparedStatement stmt = this.conn.prepareStatement(sql);
            stmt.setString(1, ambiente.getNome());
            stmt.setInt(2, ambiente.getId());
            stmt.execute();
            stmt.close();
            conn.close();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }   
    }
    public void Deletar (Objetos.Ambiente ambiente){
        try {
            conn = getConnection();
            String sql = "DELETE FROM ambiente " +
                         "WHERE \"idAmbiente\" = ? ";
            PreparedStatement stmt = this.conn.prepareStatement(sql);
            stmt.setInt(1, ambiente.getId());
            stmt.execute();
            stmt.close();
            conn.close();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }   
    }
}
