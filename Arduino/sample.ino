
int pin1 = 2;
int pin2 = 4;
int pin3 = 7;
int pin4 = 9;
int pin5 = 10;
int rele = 13;
int c;
char val;
void setup()
{
pinMode(pin1, OUTPUT);
pinMode(pin2, OUTPUT);
pinMode(pin3, OUTPUT);
pinMode(pin4, OUTPUT);
pinMode(pin5, OUTPUT);
pinMode(rele, OUTPUT);
Serial.begin(9600);
}
void loop()
{
val = Serial.read();
switch (val){
  case 'a':
    digitalWrite(pin1, !digitalRead(pin1));
    break;
  case 'b':
    digitalWrite(pin2, !digitalRead(pin2));
    break;
  case 'c':
    digitalWrite(pin3, !digitalRead(pin3));
    break;
  case 'd':
    digitalWrite(pin4, !digitalRead(pin4));
    break;
  case 'e':
    digitalWrite(pin5, !digitalRead(pin5));
    break;
  case 'f':
    digitalWrite(rele, !digitalRead(rele));
    break;
}

}


