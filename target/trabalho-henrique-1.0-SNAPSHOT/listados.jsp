<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Minha Casa</title>
        <link href="https://fonts.googleapis.com/css?family=Raleway:400,700" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="css/listados.css" />        
    </head>

    <body>
        <div id="divMaisExterna">
            <form method="GET" action="controllerV2">
            <input type="hidden"
                   name="nomeDoTratadorDePagina"
                   value="henrique.pagehandlers.Tratador_listados_jsp" />
            <% if(request.getSession().getAttribute("AUTENTICADO")=="false"){%>
            <meta http-equiv="refresh" content="0.000000001; url=login.jsp">
            <%}else{%>
                <div id="divSuperior">
                    <input type="submit" name="botaoSubmit" value="Adicionar" class="botao"/>
                    <input type="submit" name="botaoSubmit" value="Editar" class="botao"/>
                    <input type="submit" name="botaoSubmit" value="Sair" class="botao"/>
                </div>
                <br>   
                <br>   
                <span class="titulo">${sessionScope.NOME_CASA}</span>
                <br>   
                <br> 
                <span class="titulo">Ambiente</span>
                <br>
                <br>
                <div id="div2">
                    <select name="botaoSubmit">
                        <%  AcessoMySQL.AmbienteDAO sqlAmbiente = new AcessoMySQL.AmbienteDAO();
                            AcessoMySQL.CasaDAO sqlCasa = new AcessoMySQL.CasaDAO();
                            Objetos.Casa casa = new Objetos.Casa();
                            casa.setId(Integer.parseInt(request.getSession().getAttribute("CASA").toString()));
                            sqlCasa.Busca(casa);
                            for (Objetos.Ambiente ambiente : sqlAmbiente.Busca_porCasa(casa)){
                                if (request.getSession().getAttribute("AMBIENTE").equals(Integer.toString(ambiente.getId()))){
                                    %> <option value= "<% out.print(ambiente.getId()); %>" > <% out.print(ambiente.getNome()); %> </option>
                                <%
                                }
                            }
                            for (Objetos.Ambiente ambiente : sqlAmbiente.Busca_porCasa(casa)){
                                if (!request.getSession().getAttribute("AMBIENTE").equals(Integer.toString(ambiente.getId()))){
                                    %> <option value= "<% out.print(ambiente.getId()); %>" > <% out.print(ambiente.getNome()); %> </option>
                                <%
                                }
                            }
                            %>
                    </select>
                </div>
                <br>
                <input type="submit" name="botaoSubmit" value="Listar Dispositivos" class="botao"/>


                <br>
                <br>
                <br>
                <span class="titulo">Dispositivo</span>
                <br>
                <br>
                <div id="divListBox">
                    <%  AcessoMySQL.DispositivoDAO sqlDispositivo = new AcessoMySQL.DispositivoDAO();
                        Objetos.Ambiente ambiente = new Objetos.Ambiente();
                        ambiente.setId(Integer.parseInt(request.getSession().getAttribute("AMBIENTE").toString()));
                        for (Objetos.Dispositivo dispositivo : sqlDispositivo.Busca_porAmbiente(ambiente)){
                        %>  <input type="text" class="ListBox" value= "<% out.print(dispositivo.getNome()); %>" ReadOnly/>
                            <input type="submit" name="<% out.print(dispositivo.getId()); %>"
                                   value= <% if(dispositivo.getStatus()==0){
                                          %>"    Ligar"
                                          <%}else{%>
                                            "Desligar"
                                          <% } %> class="ListBox"/>
                            <br>
                        <%
                        }
                        %>
                </div>
                <%}%>
            </form>
        </div>
    </body>
</html>


