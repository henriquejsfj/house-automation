<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Minha Casa</title>
        <link href="https://fonts.googleapis.com/css?family=Raleway:400,700" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="css/cadastro.css" />        
    </head>

    <body>
        <div id="divMaisExterna">
                <div id="divSuperior">
                    <input type="submit" name="botaoSubmit" value="Sair" class="botao"/>
                </div>
                <br>   
                <br>   
                <span class="titulo">${sessionScope.NOME_CASA}</span>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <div id="divLogin">
                    <br>
                        <br>
                        <input id="tituloLogin" type="text" name="tituloLogin" class="titulo" value="Novo Usuario" readOnly/>
                        <br>
                        <br>
                        <div class="textPadrao0">
                            <input type="text" name="usuario" class="textPadrao"
                                value="" placeholder="Usuário" />
                        </div>
                        <br>
                        <div class="textPadrao0">
                            <input type="password" name="senha"  class="textPadrao"
                                value="" placeholder="Senha"/>
                    
                        </div>
                        <br>
                        <div class="textPadrao0">
                            <input type="password" name="senha2"  class="textPadrao"
                                value="" placeholder="Confirme a Senha"/>
                    
                        </div>
                        <input id="tituloLogin" type="text" name="tituloLogin" class="titulo" value="Nova Casa" readOnly/>
                        <br>
                        <br>
                        <div class="textPadrao0">
                            <input type="text" name="casaNome" class="textPadrao"
                                value="" placeholder="Nome da casa" />
                        </div>
                        <br>
                        <div class="textPadrao0">
                            <input type="text" name="casaCep" class="textPadrao"
                                value="" placeholder="CEP" />
                        </div>
                        <br>
                        <div class="textPadrao0">
                            <input type="text" name="casaRua" class="textPadrao"
                                value="" placeholder="Rua" />
                        </div>
                        <br>
                        <div class="textPadrao0">
                            <input type="text" name="casaNumero" class="textPadrao"
                                 value="" placeholder="Número" />
                        </div>
                        <br>
                        
                        
                    <input type="submit" name="botaoSubmit" value="Salvar" class="botao" id="Entrar"/>
                    <span id="erro">${sessionScope.ERRO_CADASTRO}</span>

                
                </div>
                
        </div>
    </body>
</html>


