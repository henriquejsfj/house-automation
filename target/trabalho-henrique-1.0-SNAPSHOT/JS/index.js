

window.onload = main;

function main(){
    fazerPedidoAJAX({"botao":"Query", "comando":"listar_amb"});
    var paginas = new ControleDePaginas(4,'pagina');
    var vBot = document.getElementsByName('botaoSubmit');
    for (var i=0; i<vBot.length; i++){
        switch(vBot[i].value){
            case 'Adicionar':
                vBot[i].addEventListener('click', function(){paginas.mudarParaPagina(1);});
                break;
            case 'Editar':
                vBot[i].addEventListener('click', function(){paginas.mudarParaPagina(2);});
                break;
            case 'Início':
                vBot[i].addEventListener('click', function(){paginas.mudarParaPagina(0);});
                break;
            default:
                vBot[i].addEventListener('click', enviar);
        }
    }
    var vSelect = document.getElementsByName('selectAmbiente');
    for (var i=0; i<vSelect.length; i++){
        vSelect[i].addEventListener('change', function(d){paginas.mudarParaPagina(3);ListDisp.mudarParaPagina(d.target.value)});
    }
    document.getElementsByName('dispositivoAmbiente')[1].addEventListener('change', function(d){Select.mudarParaPagina(d.target.value);});
    
};

var enviar = function (d){
    var botao = d.target.value;
    // Preparacao do pedido
    var sendData = {
        "botao" : botao
        };
    switch(botao) {
        case 'Salvar':
            sendData["ambienteNome"] = document.getElementsByName('ambienteNome')[0].value;
            break;
        case 'Criar':
            sendData["dispositivoNome"] = document.getElementsByName('dispositivoNome')[0].value;
            sendData["dispositivoComando"] = document.getElementsByName('dispositivoComando')[0].value;
            sendData["dispositivoAmbiente"] = document.getElementsByName('dispositivoAmbiente')[0].value;
        case "Excluir Ambiente":
            sendData["ambienteId"] = document.getElementsByName('ambienteId')[0].value;
            break;
        case "Salvar Ambiente":
            sendData["ambienteId"] = document.getElementsByName('ambienteId')[0].value;
            sendData["ambienteNome"] = document.getElementsByName('ambienteNome')[1].value;
        case "Salvar Dispositivo":
            sendData["dispositivoId"] = document.getElementsByName('dispositivoId')[0].value;
            sendData["dispositivoNome"] = document.getElementsByName('dispositivoNome')[1].value;
            sendData["dispositivoComando"] = document.getElementsByName('dispositivoComando')[1].value;
            sendData["dispositivoAmbiente"] = document.getElementsByName('dispositivoAmbiente')[1].value;
        case "Excluir Dispositivo":
            sendData["dispositivoId"] = document.getElementsByName('dispositivoId')[1].value;
            break;
    }
    fazerPedidoAJAX(sendData, function(x){popularCamposComRespostaJSON(x);});
    setTimeout(function(){},1000);
    fazerPedidoAJAX({"botao":"Query", "comando":"listar_amb"});
};

function fazerPedidoAJAX(objetoJSON){
    
    var stringJSON = JSON.stringify(objetoJSON);
    var objPedidoAJAX = new XMLHttpRequest();
    objPedidoAJAX.open("POST", "controller_index");
    objPedidoAJAX.setRequestHeader("Content-Type","application/json;charset=UTF-8");
    // Prepara recebimento da resposta: tipo da resposta JSON!
    objPedidoAJAX.responseType = 'json';
    
    objPedidoAJAX.onreadystatechange =
        function() {
            if(objPedidoAJAX.readyState===4 && objPedidoAJAX.status===200){
                // A resposta 'response' já é avaliada para json!
                // Ao contraro da resposta responseText.
                var respJSON = objPedidoAJAX.response;
                if (!respJSON.autenticado){
                    window.location.href = "/trabalho-henrique/login.jsp";
                }if(objetoJSON.botao === "Query"){
                    var constSelect = "";
                    for (var i=0; i<respJSON.listaAmb.length; i+=2){
                        constSelect += "<option value='"+respJSON.listaAmb[i]+"'>"+respJSON.listaAmb[i+1]+"</option>";
                    }
                    var vSelect = document.getElementsByName('selectAmbiente');
                    for (var i=0; i<vSelect.length; i++){
                        vSelect[i].innerHTML ="<option value=\"000\">Selecione Ambiente</option>"+ constSelect;
                    }
                    var vSelect = document.getElementsByName('dispositivoAmbiente');
                    for (var i=0; i<vSelect.length; i++){
                        vSelect[i].innerHTML ="<option value=\"000\">Selecione Ambiente</option>"+ constSelect;
                    }
                    document.getElementsByName('ambienteId')[0].innerHTML ="<option value=\"000\">Selecione Ambiente</option>"+ constSelect;
                    
                    var constSelect = "";
                    var constDiv = "";
                    var ExId = -1;
                    maxSelectDisp = 0;
                    maxAmbiente = 0;
                    for (var i=0; i<respJSON.listaDisp.length; i+=4){
                        if (ExId != respJSON.listaDisp[i]){
                            if (i!==0){
                                constSelect += "</select>";
                                constDiv += "</div>";
                            }
                            constSelect += "<select id='selectDispAmb"+respJSON.listaDisp[i]+"' name='dispositivoId'>";
                            constDiv += "<div id='ambiente"+respJSON.listaDisp[i]+"' >";
                            ExId = respJSON.listaDisp[i];
                        }
                        if (respJSON.listaDisp[i+1]>maxSelectDisp){
                            maxSelectDisp = respJSON.listaDisp[i+1];
                        }
                        if (respJSON.listaDisp[i]>maxAmbiente){
                            maxAmbiente = respJSON.listaDisp[i];
                        }
                        arq = respJSON.listaDisp[i+3];
                        if(listaScript.indexOf(arq)==-1){
                            listaScript.push(arq);
                        }
                        constSelect += "<option value= '"+respJSON.listaDisp[i+1]+"' > "+respJSON.listaDisp[i+2].trim()+" </option>";
                        constDiv += "<div id='"+respJSON.listaDisp[i+1]+"' name='"+respJSON.listaDisp[i+3]+"' style='cursor:pointer;' >"+
                                    "<input name='"+respJSON.listaDisp[i+3]+"' type=\"text\" class=\"ListBox\" value= '"+respJSON.listaDisp[i+2].trim()+"' ReadOnly/>"+
                                    "<input name='"+respJSON.listaDisp[i+3]+"' type=\"submit\" value=\"Ler/Controlar\" class=\"ListBox\"/>"+
                                "</div>"+
                                "<div id='dispositivo"+respJSON.listaDisp[i+1]+"' style='display:none;'></div>"+
                                "<br>";
                    }
                    constSelect += "</select>";
                    constDiv += "</div>";
                    document.getElementById("criarSelect").innerHTML = constSelect;
                    document.getElementById("divListBox").innerHTML = constDiv;
                    Select = new ControleDePaginas(maxSelectDisp+1, 'selectDispAmb');
                    ListDisp = new ControleDePaginas(maxAmbiente+1,'ambiente');
                    for (var i=0; i<listaScript.length; i++){
                        var script = document.createElement('script');
                        script.src = 'JS/'+listaScript[i]+'.js'; 
                        script.onload = function () {
                            iniciar(); /// TODOS OS DISPOSITIVOS DEVEM TER A FUNÇÃO INICIAR()
                        };
                        document.head.appendChild(script);
                        
                    }
                }
                //window.location.href = "/trabalho-henrique/index.jsp";
                    
            };
        };
        
    // Envio do pedido
    objPedidoAJAX.send(stringJSON);
};
