iniciar();

function iniciar(){
    // A DIV DA LÂMPADA FOI FEITA NO HTML NORMAL E DEPOIS COPIADA PARA O JAVASCRIPT ;))
    var vetorL = document.getElementsByName('lampada');
    for (var i=0;i<vetorL.length;i++){
        vetorL[i].name = 'lampada';
        if (vetorL[i].parentNode.name!='lampada'){
            document.getElementById('dispositivo'+vetorL[i].id).innerHTML = '<input type="submit" name="dispositivo'+vetorL[i].id+'" value="">';
            vetorL[i].addEventListener('click', inverter);
            document.getElementsByName('dispositivo'+vetorL[i].id)[0].addEventListener('click', inverter);
        }
    }
}


function inverter(d){
    var sendData = {
        "driver" : 'henrique.drivers.Lampada'
        };
    if (d.target.parentNode.name=='lampada'){
        sendData["botao"] = '';
        sendData["dispositivoId"] = d.target.parentNode.id;
        if (document.getElementById('dispositivo'+d.target.parentNode.id).style.display=='none'){
            document.getElementById('dispositivo'+d.target.parentNode.id).style.display = 'block';
        }else{
            document.getElementById('dispositivo'+d.target.parentNode.id).style.display = 'none';
        }
    }else if(d.target.name=='lampada'){
        sendData["botao"] = '';
        sendData["dispositivoId"] = d.target.id;
        if (document.getElementById('dispositivo'+d.target.id).style.display=='none'){
            document.getElementById('dispositivo'+d.target.id).style.display = 'block';
        }else{
            document.getElementById('dispositivo'+d.target.id).style.display = 'none';
        }
    }else{
        sendData["botao"] = d.target.value;
        sendData["dispositivoId"] = d.target.name.slice(11,d.target.name.length);
    }
    fazerPedido(sendData);
}
function fazerPedido(objetoJSON){
    
    var stringJSON = JSON.stringify(objetoJSON);
    var objPedidoAJAX = new XMLHttpRequest();
    objPedidoAJAX.open("POST", "drivers");
    objPedidoAJAX.setRequestHeader("Content-Type","application/json;charset=UTF-8");
    // Prepara recebimento da resposta: tipo da resposta JSON!
    objPedidoAJAX.responseType = 'json';
    
    objPedidoAJAX.onreadystatechange =
        function() {
            if(objPedidoAJAX.readyState===4 && objPedidoAJAX.status===200){
                // A resposta 'response' já é avaliada para json!
                // Ao contraro da resposta responseText.
                var respJSON = objPedidoAJAX.response;
                if (respJSON.status==1){
                    document.getElementsByName("dispositivo"+respJSON.idDispositivo)[0].value='Desligar';
                }else if(respJSON.status==0){
                    document.getElementsByName("dispositivo"+respJSON.idDispositivo)[0].value='Ligar';
                }
            };
        };
    // Envio do pedido
    objPedidoAJAX.send(stringJSON);
}




