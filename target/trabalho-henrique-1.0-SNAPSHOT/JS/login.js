

window.onload = main;

function main(){
    var paginas = new ControleDePaginas(2,'pagina');
    document.getElementById('Entrar').addEventListener('click', enviar);
    document.getElementById('CriarNovo').addEventListener('click', function(){paginas.mudarParaPagina(1);});
    document.getElementById('Sair').addEventListener('click', function(){paginas.mudarParaPagina(0);});
    document.getElementById('Salvar').addEventListener('click', enviar);
    
}

var enviar = function (d){
    var botao = d.target.value;
    var ok = true;
    var sendData = {
        "botao" : botao
        };
    switch(botao) {
        case 'Entrar':
            sendData["usuario"] = document.getElementsByName('usuario')[0].value;
            sendData["senha"] = document.getElementsByName('senha')[0].value;
            break;
        case 'Salvar':
            sendData["usuario"] = document.getElementsByName('usuario')[1].value;
            sendData["senha"] = document.getElementsByName('senha')[1].value;
            sendData["senha2"] = document.getElementsByName('senha2')[0].value;
            if (sendData.senha!==sendData.senha2){
                document.getElementById('erroCadastro').innerHTML = "Senhas devem ser iguais.";
                ok = false;
            }
            sendData["casaNome"] = document.getElementsByName('casaNome')[0].value;
            sendData["casaCep"] = document.getElementsByName('casaCep')[0].value;
            sendData["casaRua"] = document.getElementsByName('casaRua')[0].value;
            sendData["casaNumero"] = document.getElementsByName('casaNumero')[0].value;

    }if (ok){
    fazerPedidoAJAX(sendData,    //popularCamposComRespostaJSON
                    function(x){popularCamposComRespostaJSON(x);}
    );}
};

function fazerPedidoAJAX(objetoJSON){
    
    var stringJSON = JSON.stringify(objetoJSON);
    var objPedidoAJAX = new XMLHttpRequest();
    objPedidoAJAX.open("POST", "controller_login");
    objPedidoAJAX.setRequestHeader("Content-Type","application/json;charset=UTF-8");
    // Prepara recebimento da resposta: tipo da resposta JSON!
    objPedidoAJAX.responseType = 'json';
    
    objPedidoAJAX.onreadystatechange =
        function() {
            if(objPedidoAJAX.readyState===4 && objPedidoAJAX.status===200){
                // A resposta 'response' já é avaliada para json!
                // Ao contraro da resposta responseText.
                var respJSON = objPedidoAJAX.response;
                
                document.getElementById('erroCadastro').innerHTML = respJSON.erro_cadastro;
                if (respJSON.erro_login){
                    document.getElementById('erroLogin').innerHTML = "Usuário ou senha incorretos";
                }if (respJSON.autenticado){
                    window.location.href = "/trabalho-henrique/index.jsp";
                }
            };
        };
        
    // Envio do pedido
    objPedidoAJAX.send(stringJSON);
};
