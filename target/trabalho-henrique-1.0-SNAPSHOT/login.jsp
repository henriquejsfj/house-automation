<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Minha Casa - Login</title>
        <link href="https://fonts.googleapis.com/css?family=Raleway:400,700" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="css/Login.css" />
        <script src="JS/utilidades.js"></script>
        <script src="JS/login.js"></script>
    </head>

    <body>
        <div id="pagina0" class="divMaisExterna">              
                <br>   
                <br>   
                <br>   
                <span class="titulo">Bem-vindo ao portal de gerenciamento do seu lar!</span>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <div class="divLogin">
                    <br>
                        <br>
                        <input id="tituloLogin" type="text" name="tituloLogin" class="titulo" value="Login" readOnly/>
                        <br>
                        <br>
                        <div id="div1" class="textPadrao0">
                            <input id="usuario" type="text" name="usuario" class="textPadrao"
                                value="" placeholder="Usuário" />
                        </div>
                        <br>
                        <div id="div2" class="textPadrao0">
                            <input id="senha" type="password" name="senha"  class="textPadrao"
                                value="" placeholder="Senha"/>
                    
                        </div>
                        <br>
                    <input type="submit" name="botaoSubmit" value="Entrar" class="botao" id="Entrar"/>
                    <input type="submit" name="botaoSubmit" value="+ Criar Novo" class="botao" id="CriarNovo"/>
                    <span id="erroLogin" class="erro"></span>

                </div>
        </div>
        <div id="pagina1" class="divMaisExterna">
                <div id="divSuperior">
                    <input id="Sair" type="submit" name="botaoSubmit" value="Sair" class="botao"/>
                </div>
                <br>   
                <br>   
                <span class="titulo">Novo Cadastro</span>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <div class="divLogin">
                    <br>
                        <br>
                        <input id="tituloLogin" type="text" name="tituloLogin" class="titulo" value="Novo Usuario" readOnly/>
                        <br>
                        <br>
                        <div class="textPadrao0">
                            <input type="text" name="usuario" class="textPadrao"
                                value="" placeholder="Usuário" />
                        </div>
                        <br>
                        <div class="textPadrao0">
                            <input type="password" name="senha"  class="textPadrao"
                                value="" placeholder="Senha"/>
                    
                        </div>
                        <br>
                        <div class="textPadrao0">
                            <input type="password" name="senha2"  class="textPadrao"
                                value="" placeholder="Confirme a Senha"/>
                    
                        </div>
                        <input id="tituloLogin" type="text" name="tituloLogin" class="titulo" value="Nova Casa" readOnly/>
                        <br>
                        <br>
                        <div class="textPadrao0">
                            <input type="text" name="casaNome" class="textPadrao"
                                value="" placeholder="Nome da casa" />
                        </div>
                        <br>
                        <div class="textPadrao0">
                            <input type="text" name="casaCep" class="textPadrao"
                                value="" placeholder="CEP" />
                        </div>
                        <br>
                        <div class="textPadrao0">
                            <input type="text" name="casaRua" class="textPadrao"
                                value="" placeholder="Rua" />
                        </div>
                        <br>
                        <div class="textPadrao0">
                            <input type="text" name="casaNumero" class="textPadrao"
                                 value="" placeholder="Número" />
                        </div>
                        <br>
                        
                        
                    <input id="Salvar" type="submit" name="botaoSubmit" value="Salvar" class="botao"/>
                    <br>
                    <span id="erroCadastro" class="erro"></span>

                
                </div>
                
        </div>
    </body>
</html>

